type ApiResult<T> = {
  data: T

  meta: {
    currentPage?: number
    perPage?: number
    lastPage?: number;
    total: number;
  }
}

export default ApiResult
