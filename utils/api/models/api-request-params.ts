export default interface RequestParams {
    perPage?: number
    page?: number
    sort?: string
}