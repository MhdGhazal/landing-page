import ApiErrorType from '../enums/api-error-type'

export type ApiError = {
  data?: any,
  message?: string,
  code?: number,
  errorType: ApiErrorType,
  errors?:any
}