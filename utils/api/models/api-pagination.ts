export default interface Pagination {
    currentPage: number
    perPage: number
    pageCount?: number
    totalCount?: number
}