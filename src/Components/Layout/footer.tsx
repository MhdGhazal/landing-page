import { Row, Col, Image } from "antd";
const Footer = () => {
  return (
    <Row justify="space-between" className="footer" style={{ width: "100%" }}>
      <div className="container">
        <Row
          justify="space-between"
          style={{ width: "100%" }}
          className="row-s"
        >
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={7}
            style={{
              display: "flex",
              flexDirection: "column",
              alignContent: "end",
              justifyContent: "center",
            }}
          >
            {/* <Row
              justify="start"
              style={{ alignItems: "center", marginBottom: 10 }}
            >
              <p
                style={{
                  textAlign: "right",
                  fontSize: 16,
                  fontWeight: 600,
                  fontFamily: "Cairo",
                  marginBottom: "0px",
                  marginLeft: 6,
                }}
              >
                دمشق المزة ,موقف الرازي
              </p>
              <i
                className="fas fa-map-marker-alt fa-fw footer-icon-s"
                style={{ marginLeft: 6 }}
              ></i>
            </Row>
            <Row justify="start" style={{ alignItems: "center" }}>
              <p
                style={{
                  textAlign: "right",
                  fontSize: 16,
                  fontWeight: 600,
                  fontFamily: "Cairo",
                  marginBottom: "0px",
                }}
              >
                10:00 اوقات العمل من الساعة <br></br>
              </p>
              <i
                className="far fa-clock fa-fw footer-icon-s"
                style={{ marginLeft: 6 }}
              ></i>
            </Row> */}
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={7}
            style={{
              display: "flex",
              flexDirection: "column",
              alignContent: "center",
              justifyContent: "center",
            }}
          >
            <p
              style={{
                fontFamily: "Cairo",
                fontSize: 20,
                color: "#00b1c5",
                fontWeight: 800,
                textAlign: "right",
              }}
            >
              نظام مسار الطبي
            </p>
            <p
              style={{
                textAlign: "right",
                fontWeight: 600,
                fontFamily: "Cairo",
              }}
            >
              طريقك الأقصر إلى الملف الصحّي الموحّد في سوريا
            </p>
          </Col>
        </Row>
      </div>
    </Row>
  );
};

export default Footer;
