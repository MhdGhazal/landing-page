import { Row, Col, Button, Image } from "antd";
import Link from "next/link";
const NavBar = () => {
  return (
    <Row style={{ width: "100%", backgroundColor: "white", zIndex: 2 }}>
      <div className="container">
        <div style={{ width: "100%" }} className="navbar">
          <div className="navbar-left">
            <div className="navbar-div-ul">
              <ul>
                <Link href="#contact" passHref={true}>
                  <li className="navbar-p-border">
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      تواصل معنا
                    </span>
                  </li>
                </Link>
                <Link href="#member" passHref={true}>
                  <li className="navbar-p-border">
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      الاشتراكات
                    </span>
                  </li>
                </Link>
                <Link href="#sections" passHref={true}>
                  <li className="navbar-p-border">
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      الأقسام
                    </span>
                  </li>
                </Link>

                <Link href="#features" passHref={true}>
                  <li className="navbar-p-border">
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      الميزات
                    </span>
                  </li>
                </Link>
                <Link href="#about" passHref={true}>
                  <li className="navbar-p-border">
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      من نحن{" "}
                    </span>
                  </li>
                </Link>
                <Link href="#" passHref={true}>
                  <li className="navbar-p-border">
                    {" "}
                    <span
                      style={{
                        fontFamily: "Cairo",
                        fontWeight: 600,
                        color: "black",
                      }}
                    >
                      {" "}
                      الرئيسية{" "}
                    </span>
                  </li>
                </Link>
              </ul>
            </div>
          </div>
          <div className="navbar-div-image-logo">
            <p
              style={{
                fontSize: 15,
                fontFamily: "Cairo",
                fontWeight: 800,
                color: "#00b1c5",
                marginBottom: 3,
              }}
            >
              نظام مسار الصحي
            </p>
          </div>
        </div>
      </div>
    </Row>
  );
};

export default NavBar;
