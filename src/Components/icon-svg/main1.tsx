const Main3 = () => {
  return (
    <>
      <svg
        width="116"
        height="104"
        viewBox="0 0 116 104"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M115 1V80.7143L63.6364 103L13.9848 80.7143V11.2857H2L13.9848 1H115Z"
          fill="#828EF7"
        />
        <path
          d="M13.9848 1H115V80.7143L63.6364 103L13.9848 80.7143V11.2857M13.9848 1V11.2857M13.9848 1L2 11.2857H13.9848"
          stroke="#828EF7"
        />
      </svg>
    </>
  );
};

export default Main3;
