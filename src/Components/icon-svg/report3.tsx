const Report3 = () => {
  return (
    <>
      <svg
        width="60"
        height="60"
        viewBox="0 0 70 95"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M69.4109 4.30957V90.6904C69.4109 93.0741 67.485 95 65.1014 95H4.30957C1.92589 94.9842 0 93.0583 0 90.6904V4.30957C0 1.92589 1.92589 0 4.30957 0H65.1171C67.485 0 69.4109 1.92589 69.4109 4.30957Z"
          fill="url(#paint0_linear_73_1166)"
        />
        <path
          d="M64.9891 7.94539V87.0389C64.9891 89.2215 63.2969 90.9849 61.2024 90.9849H7.78599C5.6915 90.9705 3.99927 89.2071 3.99927 87.0389V7.94539C3.99927 5.76281 5.6915 3.99939 7.78599 3.99939H61.2163C63.2969 3.99939 64.9891 5.76281 64.9891 7.94539Z"
          fill="url(#paint1_linear_73_1166)"
        />
        <path
          d="M26.8678 18.2486V22.1793C26.8678 22.4635 26.6468 22.6845 26.3626 22.6845H21.5479C21.2637 22.6845 21.0427 22.9055 21.0427 23.1896V28.0044C21.0427 28.2885 20.8217 28.5095 20.5376 28.5095H16.6069C16.3227 28.5095 16.1017 28.2885 16.1017 28.0044V23.1896C16.1017 22.9055 15.8807 22.6845 15.5966 22.6845H10.7976C10.5135 22.6845 10.2925 22.4635 10.2925 22.1793V18.2486C10.2925 17.9645 10.5135 17.7435 10.7976 17.7435H15.6124C15.8965 17.7435 16.1175 17.5225 16.1175 17.2383V12.4236C16.1175 12.1395 16.3385 11.9185 16.6227 11.9185H20.5534C20.8375 11.9185 21.0585 12.1395 21.0585 12.4236V17.2383C21.0585 17.5225 21.2795 17.7435 21.5637 17.7435H26.3626C26.6468 17.7435 26.8678 17.9645 26.8678 18.2486Z"
          fill="url(#paint2_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 39.6701H10.8449C10.5292 39.6701 10.2766 39.4175 10.2766 39.1018V37.2864C10.2766 36.9707 10.5292 36.7181 10.8449 36.7181H58.5659C58.8816 36.7181 59.1342 36.9707 59.1342 37.2864V39.1018C59.1184 39.4175 58.8816 39.6701 58.5659 39.6701Z"
          fill="url(#paint3_linear_73_1166)"
        />
        <path
          d="M58.5659 17.0489H34.8869C34.5712 17.0489 34.3186 16.7963 34.3186 16.4806V14.681C34.3186 14.3652 34.5712 14.1127 34.8869 14.1127H58.5659C58.8816 14.1127 59.1342 14.3652 59.1342 14.681V16.4964C59.1184 16.7963 58.8816 17.0489 58.5659 17.0489Z"
          fill="url(#paint4_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M51.5569 24.7208H34.8869C34.5712 24.7208 34.3186 24.4682 34.3186 24.1525V22.3371C34.3186 22.0214 34.5712 21.7688 34.8869 21.7688H51.5569C51.8726 21.7688 52.1252 22.0214 52.1252 22.3371V24.1525C52.1094 24.4682 51.8726 24.7208 51.5569 24.7208Z"
          fill="url(#paint5_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 48.2893H10.8449C10.5292 48.2893 10.2766 48.0367 10.2766 47.721V45.9056C10.2766 45.5899 10.5292 45.3373 10.8449 45.3373H58.5659C58.8816 45.3373 59.1342 45.5899 59.1342 45.9056V47.721C59.1184 48.0367 58.8816 48.2893 58.5659 48.2893Z"
          fill="url(#paint6_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 56.9085H10.8449C10.5292 56.9085 10.2766 56.6559 10.2766 56.3402V54.5248C10.2766 54.2091 10.5292 53.9565 10.8449 53.9565H58.5659C58.8816 53.9565 59.1342 54.2091 59.1342 54.5248V56.356C59.1184 56.6559 58.8816 56.9085 58.5659 56.9085Z"
          fill="url(#paint7_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 65.5434H10.8449C10.5292 65.5434 10.2766 65.2908 10.2766 64.9751V63.1597C10.2766 62.844 10.5292 62.5914 10.8449 62.5914H58.5659C58.8816 62.5914 59.1342 62.844 59.1342 63.1597V64.9751C59.1184 65.2908 58.8816 65.5434 58.5659 65.5434Z"
          fill="url(#paint8_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 74.1626H10.8449C10.5292 74.1626 10.2766 73.91 10.2766 73.5943V71.7789C10.2766 71.4632 10.5292 71.2106 10.8449 71.2106H58.5659C58.8816 71.2106 59.1342 71.4632 59.1342 71.7789V73.5943C59.1184 73.91 58.8816 74.1626 58.5659 74.1626Z"
          fill="url(#paint9_linear_73_1166)"
        />
        <path
          opacity="0.5"
          d="M58.5659 82.7817H10.8449C10.5292 82.7817 10.2766 82.5291 10.2766 82.2134V80.398C10.2766 80.0823 10.5292 79.8297 10.8449 79.8297H58.5659C58.8816 79.8297 59.1342 80.0823 59.1342 80.398V82.2134C59.1184 82.5291 58.8816 82.7817 58.5659 82.7817Z"
          fill="url(#paint10_linear_73_1166)"
        />
        <path
          opacity="0.1"
          d="M69.4109 4.30957V90.6904C69.4109 93.0741 67.485 95 65.1014 95H4.30957C1.92589 94.9842 0 93.0583 0 90.6904V89.1908C37.0813 73.8784 54.3511 15.1545 58.2503 0H65.1014C67.485 0 69.4109 1.92589 69.4109 4.30957Z"
          fill="#1A6FB5"
          fillOpacity="0.4"
        />
        <defs>
          <linearGradient
            id="paint0_linear_73_1166"
            x1="94.9842"
            y1="125.979"
            x2="-5.99901"
            y2="-5.49908"
            gradientUnits="userSpaceOnUse"
          >
            <stop offset="0.3061" stopColor="#5BA7FC" />
            <stop offset="1" stopColor="#69B8FC" />
          </linearGradient>
          <linearGradient
            id="paint1_linear_73_1166"
            x1="8.6226"
            y1="7.04452"
            x2="57.9288"
            y2="78.0184"
            gradientUnits="userSpaceOnUse"
          >
            <stop offset="0.3061" stopColor="white" />
            <stop offset="1" stopColor="white" />
          </linearGradient>
          <linearGradient
            id="paint2_linear_73_1166"
            x1="9.99837"
            y1="9.49848"
            x2="24.9959"
            y2="33.9944"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#B0F4FF" />
            <stop offset="0.368525" stopColor="#80E4FD" />
            <stop offset="1" stopColor="#6CDDFC" />
          </linearGradient>
          <linearGradient
            id="paint3_linear_73_1166"
            x1="10.2888"
            y1="38.2008"
            x2="59.1256"
            y2="38.2008"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint4_linear_73_1166"
            x1="34.319"
            y1="15.5818"
            x2="59.1256"
            y2="15.5818"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint5_linear_73_1166"
            x1="34.319"
            y1="23.2576"
            x2="52.1168"
            y2="23.2576"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint6_linear_73_1166"
            x1="10.2888"
            y1="46.8239"
            x2="59.1256"
            y2="46.8239"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint7_linear_73_1166"
            x1="10.2888"
            y1="55.4471"
            x2="59.1256"
            y2="55.4471"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint8_linear_73_1166"
            x1="10.2888"
            y1="64.0701"
            x2="59.1256"
            y2="64.0701"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint9_linear_73_1166"
            x1="10.2888"
            y1="72.6931"
            x2="59.1256"
            y2="72.6931"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
          <linearGradient
            id="paint10_linear_73_1166"
            x1="10.2888"
            y1="81.3161"
            x2="59.1256"
            y2="81.3161"
            gradientUnits="userSpaceOnUse"
          >
            <stop stopColor="#6CBCFC" />
            <stop offset="1" stopColor="#458DFC" />
          </linearGradient>
        </defs>
      </svg>
    </>
  );
};

export default Report3;
