const Hospital = () => {
  return (
    <>
      <svg
        width="79"
        height="79"
        viewBox="0 0 79 79"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <rect width="79" height="79" fill="url(#pattern0)" />
        <defs>
          <pattern
            id="pattern0"
            patternContentUnits="objectBoundingBox"
            width="1"
            height="1"
          >
            <use transform="scale(0.00195312)" />
          </pattern>
          <image id="image0_73_780" width="512" height="512" />
        </defs>
      </svg>
    </>
  );
};

export default Hospital;
