import {
  ArrowLeftOutlined,
  TeamOutlined,
  FacebookFilled,
  CheckSquareOutlined,
} from "@ant-design/icons";
import { Row, Col, Image, Button } from "antd";
import Modal from "./Modal";

import Main3 from "../icon-svg/main3";
import Main from "../icon-svg/main";
import Main2 from "../icon-svg/main1";
import Images from "next/image";
import { useState } from "react";
import Hospital from "../icon-svg/hospital";
const Features = () => {
  const [Clinic, setClinic] = useState<boolean>(false);

  const ClinickHandler = () => {
    setClinic(false);
  };
  const CenterHandler = () => {
    setClinic(true);
  };

  const [bool, setBool] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [type, setType] = useState<string>("");

  const popupHandler = () => {
    setBool(false);
  };

  const HandelPopup = (title: string, type: string) => {
    setBool(true);
    setTitle(title);
    setType(type);
  };

  return (
    <div className="container">
      <div className="features margin-system " id="member">
        <Row style={{ width: "100%" }} className="features-content-t">
          <Row className="features-content-header">
            <div className="features-p " style={{ marginBottom: 10 }}>
              <p> الاشتراكات</p>
            </div>
          </Row>

          <Row
            justify="center"
            style={{
              width: "100%",
              flexDirection: "row-reverse",
              marginBottom: "30px",
            }}
          >
            <Button
              className={!Clinic ? "flib" : "flab"}
              onClick={ClinickHandler}
            >
              عيادة
            </Button>
            <Button
              className={Clinic ? "flib" : "flab"}
              onClick={CenterHandler}
            >
              مركز صحي
            </Button>
          </Row>
          {!Clinic && (
            <Row
              style={{
                width: "100%",
                justifyContent: "space-between",
                display: "flex",
                marginBottom: 30,
              }}
              className="member-content-row "
            >
              <Col
                xs={24}
                sm={11}
                md={11}
                lg={7}
                className="member-content"
                data-aos="fade-right"
                data-aos-delay="0"
                data-aos-duration="800"
                data-aos-easing="ease-in-out-cubic"
              >
                <Row
                  justify="center"
                  style={{
                    alignItems: "center",
                    width: "100%",
                  }}
                  className="member-content-header"
                >
                  <p>الاشتراك الأساسي</p>
                  <Row className="flag">
                    <Main3></Main3>
                  </Row>
                  <Row className="flag">
                    <Main3></Main3>
                    <p className="flag-font">مجاني</p>
                  </Row>
                </Row>
                <Row className="ppp">
                  <p>الباقة الأساسية لتبدء رحلتك مع مسار ... مجانية</p>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    إدارة متكاملة لحجز المواعيد
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    ربط العيادة بتطبيق صحتي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الربط الإلكتروني بشركات التأمين
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    صياغة القصة المرضية لكل مريض
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الوصول لمحتويات لمكتبة النماذج
                    <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                      {" "}
                      +10 نموذج
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    المنصات التي يعمل عليها النظام
                    <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                      {" "}
                      الحاسب والموبايل
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إضافة{" "}
                    <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                      طبيب واحد
                    </span>{" "}
                    ضمن العيادة الواحدة
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active disable">
                    إدارة بروفايلات التذكير للمرضى
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                    className="disable"
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active disable">
                    استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                    className="disable"
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active disable">
                    إنشاء الوصفة الطبية لكل مريض وطباعتها
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#00b1c5" }}
                    className="disable"
                  ></CheckSquareOutlined>
                </Row>

                <Row
                  justify="center"
                  style={{
                    width: "100%",
                    marginTop: "50px",
                    marginBottom: "30px",
                  }}
                >
                  <Button
                    className="member-btn"
                    onClick={() => HandelPopup("الاشتراك الأساسي", "العيادة")}
                  >
                    {" "}
                    اشترك الآن
                  </Button>
                </Row>
              </Col>

              <Col
                xs={24}
                sm={11}
                md={11}
                lg={7}
                className="member-content"
                data-aos="fade-right"
                data-aos-delay="0"
                data-aos-duration="800"
                data-aos-easing="ease-in-out-cubic"
              >
                <Row
                  justify="center"
                  style={{
                    alignItems: "center",
                    width: "100%",
                  }}
                  className="member-content-header"
                >
                  <p>الاشتراك المميز</p>

                  <Row className="flag">
                    <Main2></Main2>
                    <p className="flag-fonts" style={{ direction: "rtl" }}>
                      100,000 ل.س <br></br>
                      <span
                        style={{ fontSize: 13, position: "absolute", left: 50 }}
                      >
                        {" "}
                        / سنوياً
                      </span>
                    </p>
                  </Row>
                </Row>
                <Row className="ppp">
                  <p style={{ borderBottom: "5px solid #828EF7" }}>
                    باقة تتميز بالكثير من الأدوات العملية{" "}
                  </p>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    إدارة متكاملة لحجز المواعيد
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    ربط العيادة بتطبيق صحتي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الربط الإلكتروني بشركات التأمين
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    صياغة القصة المرضية لكل مريض
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الوصول لمحتويات لمكتبة النماذج
                    <span style={{ color: "#828EF7", fontWeight: 600 }}>
                      {" "}
                      +15 نموذج
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    المنصات التي يعمل عليها النظام
                    <span style={{ color: "#828EF7", fontWeight: 600 }}>
                      {" "}
                      الحاسب والموبايل
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إضافة{" "}
                    <span style={{ color: "#828EF7", fontWeight: 600 }}>
                      {" "}
                      أكثر من طبيب
                    </span>{" "}
                    ضمن العيادة الواحدة
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إدارة بروفايلات التذكير للمرضى
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إنشاء الوصفة الطبية لكل مريض وطباعتها
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#828EF7" }}
                  ></CheckSquareOutlined>
                </Row>

                <Row
                  justify="center"
                  style={{ width: "100%", marginTop: "50px", marginBottom: 30 }}
                >
                  <Button
                    className="member-btn"
                    style={{ backgroundColor: "#828EF7" }}
                    onClick={() => HandelPopup("الاشتراك المميز", "العيادة")}
                  >
                    {" "}
                    اشترك الآن
                  </Button>
                </Row>
              </Col>

              <Col
                xs={24}
                sm={11}
                md={11}
                lg={7}
                className="member-content"
                data-aos="fade-right"
                data-aos-delay="0"
                data-aos-duration="800"
                data-aos-easing="ease-in-out-cubic"
              >
                <Row
                  justify="center"
                  style={{
                    alignItems: "center",
                    width: "100%",
                  }}
                  className="member-content-header"
                >
                  <p>الاشتراك الخاص</p>
                  <Row className="flag">
                    <Main></Main>
                    <p className="flag-fonts" style={{ direction: "rtl" }}>
                      150,000 ل.س <br></br>
                      <span
                        style={{ fontSize: 13, position: "absolute", left: 50 }}
                      >
                        {" "}
                        / سنوياً
                      </span>
                    </p>
                  </Row>
                </Row>
                <Row className="ppp">
                  <p style={{ borderBottom: "5px solid #3B6BE8" }}>
                    باقة تمنحك كافة ميزات مسار
                  </p>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    إدارة متكاملة لحجز المواعيد
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 5,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 7,
                  }}
                >
                  <p className="member-content-active">
                    ر بط العيادة بتطبيق صحتي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الربط الإلكتروني بشركات التأمين
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    صياغة القصة المرضية لكل مريض
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active">
                    الوصول لمحتويات لمكتبة النماذج
                    <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                      {" "}
                      +50 نموذج
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    المنصات التي يعمل عليها النظام
                    <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                      {" "}
                      الحاسب والموبايل
                    </span>
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إضافة{" "}
                    <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                      {" "}
                      أكثر من طبيب
                    </span>{" "}
                    ضمن العيادة الواحدة
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إدارة بروفايلات التذكير للمرضى
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  style={{
                    marginTop: 3,
                    width: "100%",
                    textAlign: "center",
                    alignItems: "flex-start",
                    padding: 5,
                    flexWrap: "nowrap",
                    justifyContent: "flex-end",
                    gap: 5,
                  }}
                >
                  <p className="member-content-active ">
                    إنشاء الوصفة الطبية لكل مريض وطباعتها
                  </p>
                  <CheckSquareOutlined
                    style={{ marginTop: 5, color: "#3B6BE8" }}
                  ></CheckSquareOutlined>
                </Row>
                <Row
                  justify="center"
                  style={{ width: "100%", marginTop: "50px", marginBottom: 30 }}
                >
                  <Button
                    className="member-btn"
                    style={{ backgroundColor: "#3B6BE8" }}
                    onClick={() => HandelPopup("الاشتراك الخاص", "العيادة")}
                  >
                    {" "}
                    اشترك الآن
                  </Button>
                </Row>
              </Col>
            </Row>
          )}
        </Row>
        {Clinic && (
          <Row
            style={{
              width: "100%",
              justifyContent: "space-between",
              display: "flex",
            }}
            className="member-content-row"
          >
            <Col
              xs={24}
              sm={11}
              md={11}
              lg={7}
              className="member-content"
              data-aos="fade-right"
              data-aos-delay="0"
              data-aos-duration="800"
              data-aos-easing="ease-in-out-cubic"
            >
              <Row
                justify="center"
                style={{
                  alignItems: "center",
                  width: "100%",
                }}
                className="member-content-header"
              >
                <p>الاشتراك الأساسي</p>
                <Row className="flag">
                  <Main3></Main3>
                  <p className="flag-font">مجاني</p>
                </Row>
              </Row>
              <Row className="ppp">
                <p>الباقة الأساسية لتبدء رحلتك مع مسار ... مجانية</p>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">
                  إدارة متكاملة لحجز المواعيد
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">ربط المركز بتطبيق صحتي</p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الربط الإلكتروني بشركات التأمين
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  صياغة القصة المرضية لكل مريض
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الوصول لمحتويات لمكتبة النماذج
                  <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                    {" "}
                    +10 نموذج
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  المنصات التي يعمل عليها النظام
                  <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                    {" "}
                    الحاسب والموبايل
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إضافة{" "}
                  <span style={{ color: "#00b1c5", fontWeight: 600 }}>
                    طبيب واحد
                  </span>{" "}
                  ضمن العيادة الواحدة
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active disable">
                  إدارة بروفايلات التذكير للمرضى
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                  className="disable"
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active disable">
                  استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                  className="disable"
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active disable">
                  إنشاء الوصفة الطبية لكل مريض وطباعتها
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#00b1c5" }}
                  className="disable"
                ></CheckSquareOutlined>
              </Row>

              <Row
                justify="center"
                style={{
                  width: "100%",
                  marginTop: "50px",
                  marginBottom: "30px",
                }}
              >
                <Button
                  className="member-btn"
                  onClick={() => HandelPopup("الاشتراك الأساسي", "المركز")}
                >
                  {" "}
                  اشترك الآن
                </Button>
              </Row>
            </Col>
            <Col
              xs={24}
              sm={11}
              md={11}
              lg={7}
              className="member-content"
              data-aos="fade-right"
              data-aos-delay="0"
              data-aos-duration="800"
              data-aos-easing="ease-in-out-cubic"
            >
              <Row
                justify="center"
                style={{
                  alignItems: "center",
                  width: "100%",
                }}
                className="member-content-header"
              >
                <p>الاشتراك المميز</p>
                <Row className="flag">
                  <Row className="flagss">
                    <Main2></Main2>
                    <p
                      className="flag-fonts"
                      style={{ direction: "rtl", top: 17 }}
                    >
                      <span
                        style={{ fontSize: 13, position: "absolute", left: 50 }}
                      >
                        ابتداءً من{" "}
                      </span>{" "}
                      <br></br> 250,000 ل.س <br></br>
                      <span
                        style={{ fontSize: 13, position: "absolute", left: 55 }}
                      >
                        {" "}
                        / سنوياً
                      </span>
                    </p>
                  </Row>
                </Row>
              </Row>
              <Row className="ppp">
                <p style={{ borderBottom: "5px solid #828EF7" }}>
                  باقة تتميز بالكثير من الأدوات العملية{" "}
                </p>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">
                  إدارة متكاملة لحجز المواعيد
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">ربط المركز بتطبيق صحتي </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الربط الإلكتروني بشركات التأمين
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  صياغة القصة المرضية لكل مريض
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الوصول لمحتويات لمكتبة النماذج
                  <span style={{ color: "#828EF7", fontWeight: 600 }}>
                    {" "}
                    +10 نموذج
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  المنصات التي يعمل عليها النظام
                  <span style={{ color: "#828EF7", fontWeight: 600 }}>
                    {" "}
                    الحاسب والموبايل
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إضافة{" "}
                  <span style={{ color: "#828EF7", fontWeight: 600 }}>
                    خمسة أطباء
                  </span>{" "}
                  ضمن العيادة الواحدة
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إدارة بروفايلات التذكير للمرضى
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إنشاء الوصفة الطبية لكل مريض وطباعتها
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#828EF7" }}
                ></CheckSquareOutlined>
              </Row>

              <Row
                justify="center"
                style={{ width: "100%", marginTop: "50px", marginBottom: 30 }}
              >
                <Button
                  className="member-btn"
                  style={{ backgroundColor: "#828EF7" }}
                  onClick={() => HandelPopup("الاشتراك المميز", "المركز")}
                >
                  {" "}
                  اشترك الآن
                </Button>
              </Row>
            </Col>
            <Col
              xs={24}
              sm={11}
              md={11}
              lg={7}
              className="member-content"
              data-aos="fade-right"
              data-aos-delay="0"
              data-aos-duration="800"
              data-aos-easing="ease-in-out-cubic"
            >
              <Row
                justify="center"
                style={{
                  alignItems: "center",
                  width: "100%",
                }}
                className="member-content-header"
              >
                <p>الاشتراك الخاص</p>
                <Row className="flag">
                  <Main></Main>
                  <p
                    className="flag-fonts"
                    style={{ direction: "rtl", top: 17 }}
                  >
                    <span
                      style={{ fontSize: 13, position: "absolute", left: 50 }}
                    >
                      ابتداءً من{" "}
                    </span>{" "}
                    <br></br>
                    350,000 ل.س <br></br>
                    <span
                      style={{ fontSize: 13, position: "absolute", left: 55 }}
                    >
                      {" "}
                      / سنوياً
                    </span>
                  </p>
                </Row>
              </Row>

              <Row className="ppp">
                <p style={{ borderBottom: "5px solid #3B6BE8" }}>
                  باقة تمنحك كافة ميزات مسار
                </p>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">
                  إدارة متكاملة لحجز المواعيد
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 5,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 7,
                }}
              >
                <p className="member-content-active">ربط المركز بتطبيق صحتي</p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الربط الإلكتروني بشركات التأمين
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  صياغة القصة المرضية لكل مريض
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active">
                  الوصول لمحتويات لمكتبة النماذج
                  <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                    {" "}
                    +50 نموذج
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  المنصات التي يعمل عليها النظام
                  <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                    {" "}
                    الحاسب والوبايل
                  </span>
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إضافة{" "}
                  <span style={{ color: "#3B6BE8", fontWeight: 600 }}>
                    عشرة أطباء
                  </span>{" "}
                  ضمن العيادة الواحدة
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إدارة بروفايلات التذكير للمرضى
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  استدعاء مريض من صالة الانتظار عن طريق خدمة الاستدعاء الآلي
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                style={{
                  marginTop: 3,
                  width: "100%",
                  textAlign: "center",
                  alignItems: "flex-start",
                  padding: 5,
                  flexWrap: "nowrap",
                  justifyContent: "flex-end",
                  gap: 5,
                }}
              >
                <p className="member-content-active ">
                  إنشاء الوصفة الطبية لكل مريض وطباعتها
                </p>
                <CheckSquareOutlined
                  style={{ marginTop: 5, color: "#3B6BE8" }}
                ></CheckSquareOutlined>
              </Row>
              <Row
                justify="center"
                style={{ width: "100%", marginTop: "50px", marginBottom: 30 }}
              >
                <Button
                  className="member-btn"
                  style={{ backgroundColor: "#3B6BE8" }}
                  onClick={() => HandelPopup("الاشتراك الخاص", "المركز")}
                >
                  {" "}
                  اشترك الآن
                </Button>
              </Row>
            </Col>
          </Row>
        )}
      </div>

      {bool && (
        <Modal
          title={title}
          type={type}
          bool={bool}
          popupHandler={popupHandler}
        ></Modal>
      )}
    </div>
  );
};

export default Features;
