import {
  ArrowLeftOutlined,
  TeamOutlined,
  DeploymentUnitOutlined,
} from "@ant-design/icons";
import { Col, Row } from "antd";
import Report from "../icon-svg/report";
import Report2 from "../icon-svg/report2";
import Report3 from "../icon-svg/report3";
import Report4 from "../icon-svg/report4";
import Smartphone from "../icon-svg/smart-phone";
import Share from "../icon-svg/share";
import { Image } from "antd";
const Section = () => {
  const array = [1, 2, 3, 4, 5, 6];
  return (
    <div className="container">
      <div className="Sections margin-system" id="sections">
        <div className="Sections-p ">
          <p> أقسام النظام </p>
        </div>
        <Row
          style={{
            width: "100%",
            justifyContent: "space-between",

            display: "flex",
          }}
          className="section-content-row"
        >
          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content"
            data-aos="fade-right"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
          >
            <Row justify="end" className="icon-section">
              {/* <Report2
               style={{ fontSize: 30, textAlign: "right", color: "#00b1c5" }}
              ></Report2> */}
              <Smartphone></Smartphone>
              <p className="section-content-title"> تطبيق صحتي</p>
            </Row>
            <Row style={{ width: "100%", justifyContent: "end" }}>
              <p
                style={{
                  color: "black",
                  fontWeight: 600,
                  fontFamily: "Cairo",
                  marginBottom: 20,
                }}
                className="content-p"
              >
                بوابة المواطنين للدخول إلى النظام، يمكن عن طريقه إنشاء الملفات
                الطبية ومتابعة معلوماتها المتجددة عن طريق مقدمي خدمات الرعاية
                الصحية، كما يستطيع من خلاله متابعة كافة الحالات المرضية السابقة
                له أو لعائلته وذلك من خلال واجهات مريحة
              </p>
            </Row>
          </Col>
          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content"
            data-aos="fade-right"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
          >
            <Row justify="end" className="icon-section">
              <Report2
              // style={{ fontSize: 30, textAlign: "right", color: "#00b1c5" }}
              ></Report2>
              <p className="section-content-title">
                {" "}
                برنامج الاستقبال والمواعيد
              </p>
            </Row>
            <Row
              justify="end"
              style={{ width: "100%", alignItems: "center" }}
              className="section-content-readmore"
            >
              <p
                style={{
                  color: "black",
                  fontFamily: "Cairo",
                  fontWeight: 600,
                  marginBottom: 20,
                }}
                className="content-p"
              >
                جزء النظام الموجه إلى موظفي الاستقبال في العيادة أو المركز
                الصحي، حيث يسمح للموظف بإضافة مواطنين جدد إلى النظام ويساعده على
                جدولة أوقات الدوام والمواعيد. كما يتم العمل من خلاله على تنسيق
                المواعيد بين الأطباء والمرضى
              </p>
            </Row>
          </Col>
          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content"
            data-aos="fade-up"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
          >
            <Row justify="end" className="icon-section">
              <Report4
              // style={{ fontSize: 30, textAlign: "right", color: "#00b1c5" }}
              ></Report4>
              <p className="section-content-title"> برنامج الطبيب</p>
            </Row>
            <Row
              justify="end"
              style={{ width: "100%", alignItems: "center" }}
              className="section-content-readmore"
            >
              <p
                style={{
                  color: "black",
                  fontFamily: "Cairo",
                  fontWeight: 600,
                  marginBottom: 20,
                }}
                className="content-p"
              >
                لبّ النظام الموجه إلى الأطباء بكافة الاختصاصات مع نماذج عالمية
                لأسئلة التشخيص المناسبة للمرضى بمختلف الحالات. كما يمكن للطبيب
                من خلاله الوصول إلى الملف الطبي الموحد لأي مريض والاطلاع
                والتعديل عليه وفق سماحيات يحددها المريض نفسه.
              </p>
            </Row>
          </Col>

          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content hidden"
            data-aos="fade-up"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
            style={{ overflow: "hidden" }}
          ></Col>
          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content"
            data-aos="fade-up"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
            style={{ overflow: "hidden" }}
          >
            <Row justify="end" style={{}} className="icon-section">
              <DeploymentUnitOutlined
                style={{ fontSize: 60, textAlign: "center", color: "#00b1c5" }}
              ></DeploymentUnitOutlined>
              <p className="section-content-title">
                {" "}
                برنامج تشارك الملفات الطبية{" "}
              </p>
            </Row>
            <Row style={{ width: "100%", justifyContent: "end" }}>
              <p
                style={{
                  color: "black",
                  fontWeight: 600,
                  fontFamily: "Cairo",
                  marginBottom: 20,
                }}
                className="content-p"
              >
                يأتي برنامج تشارك الملفات مع إمكانية إرسال واستقبال الوصفات
                والتحاليل وصور الأشعة من وإلى أي كيان طبي مشترك مع النظام. وذلك
                بغية تسهيل نقل مريض من كيان الى آخر وتقليل التعاملات الورقية قدر
                . الإمكان مما يوفر الوقت والجهد في عمليات نقل المرضى
              </p>
            </Row>
            <Row
              justify="end"
              style={{ width: "100%" }}
              className="section-content-readmore"
            ></Row>
          </Col>

          <Col
            xs={24}
            sm={11}
            md={11}
            lg={7}
            className="section-content"
            data-aos="fade-up"
            data-aos-delay="0"
            data-aos-duration="800"
            data-aos-easing="ease-in-out-cubic"
          >
            <Row justify="end" className="icon-section">
              <Report2
              // style={{ fontSize: 30, textAlign: "right", color: "#00b1c5" }}
              ></Report2>
              <p className="section-content-title"> برنامج الوصفة الطبية </p>
            </Row>
            <Row style={{ width: "100%", justifyContent: "end" }}>
              <p
                style={{
                  color: "black",
                  fontWeight: 600,
                  fontFamily: "Cairo",
                  marginBottom: 20,
                }}
                className="content-p"
              >
                مزوّداً بكود الأدوية السورية، يمكن للأطباء إنشاء الوصفات الطبية
                وفق التصاميم المعيارية للوصفات وذلك بالتعاون مع كوادر من الأطباء
                والعديد من العاملين في المجال الطبي مع ميّزة التنبيه لموانع
                الاستخدام والاختلاطات الدوائية.
              </p>
            </Row>
            <Row
              justify="end"
              style={{ width: "100%", alignItems: "center" }}
              className="section-content-readmore"
            ></Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};
export default Section;
