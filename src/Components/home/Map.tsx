import { MapContainer, Popup, TileLayer, Marker } from "react-leaflet";
import { Loading3QuartersOutlined } from "@ant-design/icons";
const Map = () => {
  return (
    <MapContainer
      center={{ lat: 33.504516, lng: 36.260741 }}
      zoom={30}
      scrollWheelZoom={false}
      style={{ width: "100%", height: "100%" }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />

      {/* <Marker position={{ lat: 33.3, lng: 36.15 }}></Marker> */}
    </MapContainer>
  );
};

export default Map;
