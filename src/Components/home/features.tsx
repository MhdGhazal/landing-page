import { Row, Col, Image, Button } from "antd";
import { useState } from "react";
const Features = () => {
  const [seeMore, setSeeMore] = useState<boolean>(false);

  const showMoreHandler = () => {
    setSeeMore(true);
  };

  const showLessHandler = () => {
    setSeeMore(false);
  };
  return (
    <div className="container  header">
      <div className="header-section2 margin-system " id="features">
        <div className="header-section2-p ">
          <p> ميزات النظام</p>
        </div>
        <Row style={{ width: "100%" }} className="header-section2-container ">
          <Col
            sm={24}
            xs={24}
            md={24}
            lg={12}
            data-aos="fade-right"
            data-aos-delay="40"
            data-aos-duration="1000"
            data-aos-easing="ease-in-out-cubic"
            style={{ overflow: "hidden" }}
            className="image-feature"
          >
            <Image
              src="/assets/tt.jpg"
              alt=""
              preview={false}
              width="100%"
              height="100%"
              style={{ objectFit: "contain" }}
            ></Image>
          </Col>
          <Col
            sm={24}
            xs={24}
            md={24}
            lg={12}
            data-aos="fade-up"
            data-aos-delay="40"
            data-aos-duration="1200"
            data-aos-easing="ease-in-out-cubic"
            className="header-section2-content"
          >
            <p style={{ textAlign: "right" }}>
              {" "}
              السجلات الطبية الإلكترونية هي مجموعة من البيانات الطبية لمريض،
              مجمعة ومخزنة بصيغة رقمية، وقابلة للتناقل والمشاركة بين المؤسسات
              العاملة في الحقل الطبي ضمن قواعد وحدود خصوصية المريض وأمن
              البيانات.
            </p>

            <p style={{ textAlign: "right" }}>
              والملف الطبي الموحد ، هو صيغة موحدة للسجلات الصحية -موحدة على
              المستوى الوطني- بحيث يكون لكل مواطن ملف طبي يبدأ من تاريخ ولادته
              وتتراكم فيه البيانات الطبية بصيغ معيارية، ويخزن مركزياً مع إمكانية
              الوصول اليه من قبل المخوّلين بسماحيات مختلفة عند الحاجة.
            </p>
            <Row
              justify="end"
              // style={{ marginTop: 30 }}
              className={!seeMore ? "" : "disappeare"}
            >
              <Button
                className="header-section1-content-btn"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onClick={showMoreHandler}
              >
                {" "}
                <p> مشاهدة المزيد</p>
              </Button>
            </Row>
            <Row justify="end" className={!seeMore ? "disappeare" : ""}>
              <p style={{ textAlign: "right" }}>
                نميّز بين السجل الطبي والسجل الصحي بأنّ السجل الصحي هو الحالة
                الأعم، وهو يشمل بالإضافة إلى بيانات السجل الطبي، بيانات
                ديموغرافية (كالعمر والحالة الاجتماعية والموقع الجغرافي)، وبيانات
                تاريخية (كالإرث المرضي للعائلة)، وهذه البيانات تساعد في وضع
                الإحصاءات واستخراج مؤشرات الأداء.
              </p>

              <Button
                className="header-section1-content-btn"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: 30,
                }}
                onClick={showLessHandler}
              >
                {" "}
                <p> مشاهدة أٌقل</p>
              </Button>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default Features;
