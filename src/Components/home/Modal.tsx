import {
  Button,
  Modal,
  Row,
  Col,
  Form,
  Input,
  Dropdown,
  Select,
  notification,
} from "antd";
import { Handler } from "leaflet";
import { FC, useState } from "react";
import { useQuery } from "react-query";
import { Subscription } from "../../modal/subscription";
import { PostContact, GetAllRegin } from "../../react-query-services/City";
import { PostSubcription } from "../../react-query-services/subscription";
import { useMutation } from "react-query";
const Moddls: FC<{
  title: string;
  type: string;
  bool: boolean;
  popupHandler: () => void;
}> = (props) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(props.bool);
  const PostSubscription = useMutation("Post", PostSubcription, {
    onSuccess: () => {
      notification.open({
        message: "شكرا لإشتراكك معنا سيتواصل فريق المبيعات معك قريبا",
      });
      handleCancel();
      form.resetFields();
    },
    onError: () => {
      notification.open({
        message: "حدث خطأ ما حاول لاحقا",
      });
      // handleCancel();
      // form.resetFields();
    },
  });

  const [id, SetId] = useState<string>("");
  const { Option } = Select;

  const [form] = Form.useForm();
  const City = useQuery("AllCity", PostContact, {});
  const Regin = useQuery(["AllRegin", id], () => GetAllRegin(id), {});

  console.log(Regin?.data?.items);
  const handleOk = () => {
    setIsModalVisible(false);
    props.popupHandler();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    props.popupHandler();
  };

  const HandlerChange = (e: any) => {
    console.log(e);
    SetId(e);
  };

  const { TextArea } = Input;

  const title = (
    <p>
      <span style={{ fontSize: 15 }}> {"(" + props.type + ")"}</span>
      <br></br> التسجيل في {props.title}
    </p>
  );

  const finish = (e: any) => {
    let s: Subscription = {
      email: "",
      ownerName: "",
      mobileNumber: "",
      regionId: 0,
      clinicArName: "",
      clinicEnName: "",
      notes: "",
      isActive: true,
      subscriptionId: 0,
    };
    s.ownerName = e.fname + " " + e.lname;
    s.email = e.email;
    s.clinicArName = e.car;
    s.clinicEnName = e.cen;
    s.isActive = true;
    s.mobileNumber = e.tel;
    s.notes = e.ask;
    s.regionId = e.regin;
    if (props.title == "الاشتراك الأساسي" && props.type == "العيادة") {
      s.subscriptionId = 2;
    }
    if (props.title == "الاشتراك المميز" && props.type == "العيادة") {
      s.subscriptionId = 3;
    }
    if (props.title == "الاشتراك الخاص" && props.type == "العيادة") {
      s.subscriptionId = 4;
    }
    if (props.title == "الاشتراك الأساسي" && props.type == "المركز") {
      s.subscriptionId = 5;
    }
    if (props.title == "الاشتراك المميز" && props.type == "المركز") {
      s.subscriptionId = 6;
    }
    if (props.title == "الاشتراك الخاص" && props.type == "المركز") {
      s.subscriptionId = 7;
    }
    PostSubscription.mutate(s);
  };

  return (
    <div className="modal">
      <Modal
        title={title}
        visible={isModalVisible}
        width={1000}
        onOk={handleOk}
        onCancel={handleCancel}
        style={{ top: 27 }}
        maskClosable={false}
        className={
          props.title == "الاشتراك الأساسي"
            ? "a modal"
            : props.title == "الاشتراك المميز"
            ? "b modal"
            : props.title == "الاشتراك الخاص"
            ? "c modal"
            : ""
        }
      >
        <Row style={{ width: "100%" }}>
          <Form style={{ width: "100%" }} onFinish={finish} form={form}>
            <Row
              style={{ width: "100%" }}
              justify="space-between"
              className="revers"
            >
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  label="الكنية"
                  name="lname"
                  rules={[
                    {
                      required: true,
                      message: "حقل الكنية مطلوب",
                    },
                  ]}
                >
                  <Input placeholder="الكنية" className="text"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  style={{ width: "100%" }}
                  label="الاسم"
                  name="fname"
                  rules={[
                    {
                      required: true,
                      message: "حقل الاسم مطلوب",
                    },
                  ]}
                >
                  <Input placeholder="الاسم" className="text"></Input>
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ width: "100%" }} justify="space-between">
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  label="رقم الهاتف"
                  name="tel"
                  rules={[
                    {
                      required: true,
                      message: "رقم الهاتف  مطلوب",
                    },
                  ]}
                >
                  <Input placeholder="(+963)" className="text"></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  style={{ width: "100%" }}
                  name="email"
                  label="البريد الإلكتروني"
                  rules={[
                    {
                      required: true,
                      message: " البريد الإالكتروني مطلوب",
                    },
                  ]}
                >
                  <Input
                    placeholder="
                    البريد الإلكتروني"
                    className="text"
                  ></Input>
                </Form.Item>
              </Col>
            </Row>
            <Row
              style={{ width: "100%" }}
              justify="space-between"
              className="revers"
            >
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  name="car"
                  label={
                    props.type == "العيادة"
                      ? "اسم العيادة بالعربي   "
                      : "اسم المركز بالعربي"
                  }
                  rules={[
                    {
                      required: true,
                      message: `${
                        props.type == "العيادة"
                          ? "  اسم العيادة بالعربي مطلوب  "
                          : "اسم المركز بالعربي مطلوب"
                      }
                      `,
                    },
                  ]}
                >
                  <Input
                    placeholder={
                      props.type == "العيادة"
                        ? "اسم العيادة بالعربي   "
                        : "اسم المركز بالعربي"
                    }
                    className="text"
                  ></Input>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item
                  name="cen"
                  label={
                    props.type == "العيادة"
                      ? "اسم العيادة بالإنلكيزي   "
                      : "اسم المركز بالإنكليزي"
                  }
                  rules={[
                    {
                      required: true,
                      message: `${
                        props.type == "العيادة"
                          ? "  اسم العيادة بالإنلكيزي مطلوب  "
                          : "اسم المركز بالإنكليزي مطلوب"
                      }
                      `,
                    },
                  ]}
                >
                  <Input
                    placeholder={
                      props.type == "العيادة"
                        ? "اسم العيادة بالإنكليزي   "
                        : "اسم المركز بالإنكليزي"
                    }
                    className="text"
                  ></Input>
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ width: "100%" }} justify="space-between">
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item
                  style={{ width: "100%" }}
                  name="regin"
                  label="المنطقة"
                  rules={[
                    {
                      required: true,
                      message: "حقل المنطقة مطلوب",
                    },
                  ]}
                >
                  <Select>
                    {Regin?.data?.items.map((ele: any) => {
                      return (
                        <Option value={ele.id} key={ele.id}>
                          {ele.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item style={{ width: "100%" }} label="المحافظة">
                  <Select defaultValue={"دمشق"} onChange={HandlerChange}>
                    {City?.data?.items.map((ele: any) => {
                      return (
                        <Option value={ele.id} key={ele.id}>
                          {ele.name}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ width: "100%" }} justify="space-between">
              <Col xs={24} sm={24} md={24} lg={24}>
                <Form.Item
                  style={{ width: "100%" }}
                  name="ask"
                  label="الاستفسار"
                  rules={[
                    {
                      required: true,
                      message: "حقل الاستفسار مطلوب",
                    },
                  ]}
                >
                  <TextArea
                    placeholder="
                   الاستفسار"
                    style={{ minHeight: 100 }}
                  ></TextArea>
                </Form.Item>
              </Col>
            </Row>
            <Row
              style={{ width: "100%" }}
              justify="space-between"
              className="revers"
            >
              <Col xs={24} sm={24} md={12} lg={12}>
                <Form.Item>
                  <Button
                    style={{
                      width: "100%",
                      padding: "20px  70px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      fontWeight: 600,
                      fontFamily: "Cairo",
                      color: "black",

                      borderRadius: 5,
                    }}
                    onClick={handleCancel}
                    className={
                      props.title == "الاشتراك الأساسي"
                        ? "aa"
                        : props.title == "الاشتراك المميز"
                        ? "bb"
                        : props.title == "الاشتراك الخاص"
                        ? "cc"
                        : ""
                    }
                  >
                    إلغاء
                  </Button>
                </Form.Item>
              </Col>
              <Col xs={24} sm={24} md={11} lg={11}>
                <Form.Item style={{ width: "100%" }}>
                  <Button
                    style={{
                      width: "100%",
                      padding: "20px 70px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      // backgroundColor: "#00b1c5",
                      color: "white",
                      fontWeight: 600,
                      fontFamily: "Cairo",
                      border: "none",
                      borderRadius: 5,
                    }}
                    className={
                      props.title == "الاشتراك الأساسي"
                        ? "a"
                        : props.title == "الاشتراك المميز"
                        ? "b"
                        : props.title == "الاشتراك الخاص"
                        ? "c"
                        : ""
                    }
                    htmlType="submit"
                    loading={PostSubscription.isLoading}
                  >
                    إرسال
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
      </Modal>
    </div>
  );
};

export default Moddls;
