import { Image, Row, Col, Button } from "antd";
import { ArrowLeftOutlined, CheckCircleFilled } from "@ant-design/icons";
import { useState } from "react";
import Link from "next/link";
const Header = () => {
  const [seeMore, setSeeMore] = useState<boolean>(false);

  const showMoreHandler = () => {
    setSeeMore(true);
  };

  const showLessHandler = () => {
    setSeeMore(false);
  };
  return (
    <div className="header margin-system">
      <div className="header-section1 margin-system container ">
        <Row
          style={{
            width: "100%",
            height: "100%",
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 30,
          }}
          className="ccc "
        >
          <Col
            xs={24}
            sm={24}
            md={14}
            lg={13}
            style={{ height: "100%" }}
            className="header-section1-img"
          >
            <Image
              src="/assets/m4.png"
              alt=""
              width="550px"
              height={400}
              style={{ objectFit: "fill" }}
              preview={false}
              className="header-section-imge-iner"
            ></Image>
          </Col>
          <Col
            xs={24}
            sm={24}
            md={24}
            lg={11}
            className="header-section1-content"
          >
            <p
              style={{ width: "100%", textAlign: "right" }}
              className="header-section1-content-p center"
            >
              نظام مسار الطبي <span>طريقك</span> الأقصر <br></br> الى الملف
              الصحّي الموحّد في سورية
            </p>
            <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 3 }}
              className="center"
            >
              <p className="pppp">تحسين جودة الرعاية الصحبة</p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row>
            <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 3 }}
              className="center"
            >
              <p className="pppp">مخزن معلومات صحية يقدم إحصائيات وطنية</p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row>

            <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 3 }}
              className="center"
            >
              <p className="pppp"> توفير بطاقة صحية إلكترونية لكل مواطن</p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row>
            <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 3 }}
              className="center"
            >
              <p className="pppp">تقليل الوفيات الناتجة عن الأخطاء الطبية</p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row>
            <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 3 }}
              className="center"
            >
              <p className="pppp">
                موجّه لجميع مواطني الجمهورية العربية السورية
              </p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row>

            {/* <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 10 }}
            >
              <p className="pppp">
                {" "}
                إمكانية مشاركة المعلومات بين مقدمي خدمات الرعاية الصحية
              </p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row> */}
            {/* <Row
              justify="end"
              style={{ alignItems: "center", marginBottom: 10 }}
            >
              <p className="pppp">تقليل الوفيات الناتجة عن الأخطاء الطبية</p>
              <CheckCircleFilled
                style={{ fontSize: 13, marginLeft: 8 }}
              ></CheckCircleFilled>
            </Row> */}

            <Row justify="end" style={{ marginTop: 50 }}>
              <Button
                className="header-section1-content-btn"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
                onClick={showMoreHandler}
              >
                {" "}
                <Link href={"#about"} passHref={true}>
                  <p style={{ fontSize: 12 }}> استكشف الان</p>
                </Link>
              </Button>
            </Row>
          </Col>
          <Row justify="center" style={{ width: "100%", position: "relative" }}>
            <Link href={"#about"} passHref={true}>
              <i
                className="fas fa-angle-double-down fa-2x header-content-icon-down"
                style={{ cursor: "pointer" }}
              ></i>
            </Link>
          </Row>
        </Row>
      </div>
      <div className="container">
        <div className="header-section2 margin-system " id="about">
          <div className="header-section2-p ">
            <p style={{ fontFamily: "Cairo", fontWeight: 600, color: "black" }}>
              {" "}
              من نحن
            </p>
          </div>
          <Row style={{ width: "100%" }} className="header-section2-container ">
            <Col
              sm={24}
              xs={24}
              md={24}
              lg={12}
              className="header-section2-content"
              data-aos="fade-right"
              data-aos-delay="40"
              data-aos-duration="1000"
              data-aos-easing="ease-in-out-cubic"
              style={{ overflow: "hidden" }}
            >
              {" "}
              <p style={{ textAlign: "right", direction: "rtl" }}>
                نطمح في نظام مسار الطبي إلى ربط جميع مقدمي خدمات الرعاية الصحية
                في الجمهورية العربية السورية ببعضهم بعضاً,يتم تخزين المعلومات
                الصحية لجميع المواطنين أنّى كان مصدرها (مركز صحي، عيادة، مخبر،
                صيدلية...) في مكان واحد يُمكّن أي مواطن من الاحتفاظ بنسخة دائمة
                من معلوماته الطبية سهلة الوصول وقابلة للمشاركة مع أي كيان طبي
                وفق سياسة خصوصية مدروسة .
              </p>
              <Row
                justify="end"
                // style={{ marginTop: 30 }}
                className={!seeMore ? "" : "disappeare"}
              >
                <Button
                  className="header-section1-content-btn"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                  onClick={showMoreHandler}
                >
                  {" "}
                  <p> مشاهدة المزيد</p>
                </Button>
              </Row>
              <Row justify="end" className={!seeMore ? "disappeare" : ""}>
                <p style={{ direction: "rtl" }}>
                  يأتي نظام مسار الطبي كثمرة عمل طويل ,وخبرة متراكمة لشركة IT
                  Land<br></br>سبق وقدمت الشركة عشرات التطبيقات والأنظمة
                  المعلوماتية التي تعمل في السوق السورية بمختلف مجالاتها، ومهّدت
                  للدخول بمشروع الملف الطبي الموحد (مسار) بالعمل على على عدّة
                  أنظمة طبية خلال السنوات السابقة
                </p>
                <Row justify="end" style={{ marginTop: 30 }}>
                  <Button
                    className="header-section1-content-btn"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                    onClick={showLessHandler}
                  >
                    {" "}
                    <p> مشاهدة أقل</p>
                  </Button>
                </Row>
              </Row>
            </Col>
            <Col
              sm={24}
              xs={24}
              md={24}
              lg={12}
              data-aos="fade-up"
              data-aos-delay="40"
              data-aos-duration="1200"
              data-aos-easing="ease-in-out-cubic"
              className="image-about"
            >
              <Image
                src="/assets/m2.png"
                alt=""
                preview={false}
                width="100%"
                height="100%"
              ></Image>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default Header;
