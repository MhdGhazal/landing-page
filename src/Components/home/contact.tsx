import { Col, Row, Form, Input, Button, Image } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { DashboardOutlined } from "@ant-design/icons";
import dynamic from "next/dynamic";
import Location from "../icon-svg/location";
import Telephone from "../icon-svg/telephone";
import Clock from "../icon-svg/clock";
import Letter from "../icon-svg/letter";
import { useMutation } from "react-query";
import { PostContact } from "../../react-query-services/Contact";
import { Contact_Req } from "../../modal/Contact";
import { notification } from "antd";
const position = [51.505, -0.09];
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useForm } from "antd/es/form/Form";
const { TextArea } = Input;

const DynamicComponentWithNoSSR = dynamic(() => import("../home/Map"), {
  ssr: false,
});

const Contact = () => {
  const ContactPost = useMutation("Contact", PostContact, {
    onSuccess: () => {
      notification.open({
        message: "شكراً لتواصلك معنا سيتم الرد على استفسارك بأسرع وقت ممكن",
      });
      form.resetFields();
    },
  });

  const finish = (e: Contact_Req) => {
    ContactPost.mutate(e);
  };
  const [form] = Form.useForm();

  return (
    <div className="container margin-system  s ">
      <div className="Sections  s" id="contact">
        <div className="Sections-p ">
          <p> تواصل معنا </p>
        </div>
      </div>
      <Row
        style={{
          width: "100%",
          justifyContent: "space-between",
        }}
        className="contact"
      >
        <Col
          xs={24}
          sm={24}
          md={10}
          lg={9}
          className="contact-left-contnet"
          data-aos="fade-right"
          data-aos-delay="0"
          data-aos-duration="800"
          data-aos-easing="ease-in-out-cubic"
        >
          <Row className="row-revers">
            <Row
              justify="end"
              style={{
                gap: 10,
                alignItems: "center",
                marginBottom: 20,
                width: "100%",
              }}
            >
              <p className="contact-left-content-p">دمشق-المزة-موقف الرازي</p>
              {/* <HomeOutlined className="contact-left-content-icon"></HomeOutlined> */}
              <Location></Location>
            </Row>
            <Row
              justify="end"
              style={{
                gap: 5,
                alignItems: "center",
                marginBottom: 20,
                width: "100%",
              }}
            >
              <p className="contact-left-content-p" style={{ marginRight: 5 }}>
                من 10 صباحاًً إلى 5 مساءً
              </p>

              <i
                className="far fa-clock fa-fw"
                style={{ fontSize: 18, color: "#00b1c5" }}
              ></i>
            </Row>
            <Row
              justify="end"
              style={{
                gap: 10,
                alignItems: "center",
                marginBottom: 20,
                width: "100%",
              }}
            >
              <p className="contact-left-content-p">0963944266058</p>
              <Telephone></Telephone>
            </Row>
            <Row
              justify="end"
              style={{
                gap: 10,
                alignItems: "center",
                marginBottom: 20,
                width: "100%",
              }}
              className="contact-left-content-top"
            >
              <p className="contact-left-content-p">itlandsy@gmail.com</p>
              <Letter></Letter>
            </Row>
            <Row
              justify="center"
              style={{ height: 250, alignItems: "center", width: "100%" }}
            >
              <DynamicComponentWithNoSSR></DynamicComponentWithNoSSR>
            </Row>
          </Row>
        </Col>

        <Col
          xs={24}
          sm={24}
          md={13}
          lg={14}
          // data-aos="fade-up"
          // data-aos-delay="0"
          // data-aos-duration="800"
          // data-aos-easing="ease-in-out-cubic"
        >
          <p style={{ textAlign: "right" }} className="cntact-content-p">
            إرسال رسالة
          </p>
          <Form onFinish={finish} form={form}>
            <Form.Item
              style={{ marginBottom: 36 }}
              name="name"
              rules={[
                {
                  required: true,
                  message: "حقل الاسم المطلوب",
                },
              ]}
            >
              <Input placeholder="الاسم" style={{ direction: "rtl" }}></Input>
            </Form.Item>
            <Form.Item
              style={{ marginBottom: 36 }}
              name="emailOrPhoneNumber"
              rules={[
                {
                  required: true,
                  message: "حقل الهاتف أو البريد الإلكتروني  مطلوب",
                },
              ]}
            >
              <Input
                placeholder="رقم الهاتف أو البريد البريد الإلكتروني"
                style={{ direction: "rtl" }}
              ></Input>
            </Form.Item>
            <Form.Item
              style={{ marginBottom: 36 }}
              name="message"
              rules={[
                {
                  required: true,
                  message: "حقل الرسالة مطلوب ",
                },
              ]}
            >
              <TextArea
                placeholder="رسالتك"
                style={{ direction: "rtl", minHeight: 220 }}
              ></TextArea>
            </Form.Item>
            <Form.Item style={{ marginBottom: 36 }}>
              <Button
                className="contact-form-button"
                htmlType="submit"
                loading={ContactPost.isLoading}
              >
                {" "}
                إرسال
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

export default Contact;
