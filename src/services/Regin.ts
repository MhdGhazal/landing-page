
import ApiService from '../../utils/api/api-service'
import {Contact_Req } from '../modal/Contact'

class Regin extends ApiService {
    constructor(config?: any) { 
        super({ baseURL: `http://45.67.221.108:5002/api/services/app/Region/`,...config });
      }
    
      public async Regin (id:any): Promise<any> {

        return this.get('GetAll?CityId='+id);
      }  
}

export const  Regins = new Regin();