
export  interface Subscription {
       ownerName: string,
        email: string,
        mobileNumber: string,
        regionId: number
        clinicArName: string,
        clinicEnName: string,
        notes: string,
        isActive: boolean,
        subscriptionId: number      
} 