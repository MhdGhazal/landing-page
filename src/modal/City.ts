
    export interface Country {
        name: string;
        isActive: boolean;
        dialCode: string;
        id: number;
    }

    export interface Item {
        name: string;
        countryId: number;
        isActive: boolean;
        country: Country;
        id: number;
    }