import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/Home.module.css";
import NavBar from "../Components/Layout/navbar";
import Header from "../Components/home/header";
import Features from "../Components/home/features";
import Sections from "../Components/home/sections";
import Member from "../Components/home/member";
import Footer from "../Components/Layout/footer";
import Contact from "../Components/home/contact";
const Home: NextPage = () => {
  return (
    <>
      <NavBar></NavBar>
      <Header></Header>
      <Features></Features>
      <Sections></Sections>
      <Member></Member>
      <Contact></Contact>
      <Footer></Footer>
    </>
  );
};

export default Home;
