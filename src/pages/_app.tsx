import "antd/dist/antd.css";
import "../../styles/globals.css";
import type { AppProps } from "next/app";
import "@fortawesome/fontawesome-free/css/all.css";
import "antd/dist/antd.css";
import AOS from "aos";
import "leaflet/dist/leaflet.css";
import "aos/dist/aos.css";
import "../../styles/global.scss";
import { QueryClient, QueryClientProvider } from "react-query";
import { useEffect } from "react";
function MyApp({ Component, pageProps }: AppProps) {
  const queryClient = new QueryClient();
  useEffect(() => {
    AOS.init();
  }, []);
  return (
    <QueryClientProvider client={queryClient}>
      <Component {...pageProps} />{" "}
    </QueryClientProvider>
  );
}

export default MyApp;
